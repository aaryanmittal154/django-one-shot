asgiref==3.5.2
black==22.8.0
certifi==2022.6.15
charset-normalizer==2.1.1
click==8.1.3
colorama==0.4.5
cssbeautifier==1.14.6
Django==4.1.1
djlint==1.12.3
docopt==0.6.2
EditorConfig==0.12.3
flake8==5.0.4
html-tag-names==0.1.2
html-void-elements==0.1.0
idna==3.3
importlib-metadata==4.12.0
jsbeautifier==1.14.6
mccabe==0.7.0
mypy-extensions==0.4.3
pathspec==0.10.1
pipreqs==0.4.11
platformdirs==2.5.2
pycodestyle==2.9.1
pyflakes==2.5.0
PyYAML==6.0
regex==2022.8.17
requests==2.28.1
six==1.16.0
sqlparse==0.4.2
tomli==2.0.1
tqdm==4.64.1
urllib3==1.26.12
yarg==0.1.9
zipp==3.8.1
