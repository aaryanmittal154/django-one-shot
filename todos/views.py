
from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# Create your views here.

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "create.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todos_list_detail", args=[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "update.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todos_list_detail", args=[self.object.id])

class TodoListDeletView(DeleteView):
    model = TodoList
    template_name = "delete.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todos_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "create_item.html"
    fields = ["task", "due_date", "is_complete", "list"]
    def get_success_url(self):
        return reverse_lazy("todos_list_detail", args=[self.object.list.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "update_item.html"
    fields = ["task", "due_date", "is_complete", "list"]
    def get_success_url(self):
        return reverse_lazy("todos_list_detail", args=[self.object.list.id])