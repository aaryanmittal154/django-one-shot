from django.contrib import admin
from todos.models import TodoList
from todos.models import TodoItem
# Register your models here.

class TodosAdmin(admin.ModelAdmin):
    pass

admin.site.register(TodoList, TodosAdmin)
admin.site.register(TodoItem)