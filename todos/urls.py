from django.urls import path

from todos.views import (TodoListCreateView)
from todos.views import (TodoListUpdateView)
from todos.views import (TodoListDeletView)
from todos.views import (TodoListListView)
from todos.views import (TodoListDetailView)
from todos.views import (TodoItemCreateView)
from todos.views import (TodoItemUpdateView)

urlpatterns =[
    path("", TodoListListView.as_view(), name = "todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todos_list_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_list_update"),
    path("<int:pk>/delete/", TodoListDeletView.as_view(), name="todos_list_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit", TodoItemUpdateView.as_view(), name="todo_item_update"),
]